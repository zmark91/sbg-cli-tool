# Build

For the build process use java 8 and maven.  

To build the cgccli tool run the following command:

```sh
mvn clean install
```
Afterwards, an executable jar file named cgccli.jar can be found in the target directory.

# Run

## List projects

```sh
java -jar cgccli.jar --token {token} projects list
```
{token}                      - The authentication token.

### Optional parameters

offset={offset}               - The zero-based starting index in the entire collection of the first item to return.  
limit={limit}                 - The maximum number of collection items to return for a single request.  
fields={fields}[,{fields}...] - Selector specifying a subset of fields to include in the response.  


## List files in project

### By project name

```sh
java -jar cgccli.jar --token {token} files list --project {project}
```

{token}                      - The authentication token.  
{project}                     - Specified in the following format: {project_owner}/{project_name}

### By parent name

```sh
java -jar cgccli.jar --token {token} files list --parent {parent}
```
{token}                            - The authentication token.  
{parent}                            - ID of the folder whose content you want to list.

### Optional parameters

offset={offset}                     - The zero-based starting index in the entire collection of the first item to return.  
limit={limit}                       - The maximum number of collection items to return for a single request.  
fields={fields}[,{fields}]          - Selector specifying a subset of fields to include in the response.  
name={names}...                     - List file with this name.  
metadata.{field}={value}...		    - Specified value in metadata field.  
origin.dataset={originDataSet}...   - List only files which are part of the dataset specified in this field.  
origin.task={originTasks}...        - List only files produced by task specified by ID in this field.  
tag={tags}...                       - List files containing this tag.   

## Get file details

```sh
java -jar cgccli.jar --token {token} files stat --file {file_id}
```

{token}                             - The authentication token.  
{file_id}                           - The file's id.  

### Optional parameters
fields={fields}[,{fields}]]         - Selector specifying a subset of fields to include in the response.  

## Update file details

```sh
java -jar cgccli.jar --token {token} files update --file {file_id} {at-least-one-optional-parametar}
```

{token}                             - The authentication token.  
{file_id}                           - The file's id.

### Optional parameters
metadata.{field}={value}...		    - Specify new value for metadata field.  
tag={tags}...                       - Specify new tags.  
name={name}                        	- Specify new file name.  

## Download file

```sh
java -jar cgccli.jar --token {token} files download --file {file_id} --dest {destination_path}
```

{token}                            - The authentication token.  
{file_id}                           - The file's id.  
{destination_path}                  - Path to the file on disk to download content in.  