package com.sbg.cli.tool.command.line;

import picocli.CommandLine;

public interface CommandLineFactory {

    CommandLine create(Object command);
}
