package com.sbg.cli.tool.command.line;

import com.google.inject.AbstractModule;
import picocli.CommandLine;

public class CommandLineModule extends AbstractModule {

    @Override
    protected void configure() {
        super.configure();

        bind(CommandLineFactory.class).to(CommandLineFactoryImpl.class);
    }

    public static class CommandLineFactoryImpl implements CommandLineFactory {

        @Override
        public CommandLine create(Object command) {
            return new CommandLine(command);
        }
    }
}
