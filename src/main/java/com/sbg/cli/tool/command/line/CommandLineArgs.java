package com.sbg.cli.tool.command.line;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;
import picocli.CommandLine;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static picocli.CommandLine.*;

@Getter
@ToString
@Builder
@Command(name = "cgccli")
public class CommandLineArgs {

    public enum Domain {projects, files}

    public enum Operation {list, stat, update, download}

    @Option(names = "--token", required = true, description = "The authentication token.")
    private String token;

    @Parameters(index = "0", description = "The domain to work upon. Valid values are: projects, files")
    private Domain domain;

    @Parameters(index = "1", description = "The operation to perform. Valid values are: list, stat, update, download")
    private Operation operation;

    @Option(names = "--file", description = "The file's id.")
    private String file;

    @Option(names = "--project", description = "Specified in the following format: {project_owner}/{project}")
    private String project;

    @Option(names = "--parent", description = "ID of the folder whose content you want to list.")
    private String parent;

    @Option(names = "--dest", description = "Destination file.")
    private String destination;

    @Option(names = "name", description = "List file with this name.")
    private List<String> names;

    @Option(names = "limit", description = "The maximum number of collection items to return for a single request.")
    private Integer limit;

    @Option(names = "offset", description = "The zero-based starting index in the entire collection of the first item to return.")
    private Integer offset;

    @Option(names = "fields", split = ",", description = "Selector specifying a subset of fields to include in the response.")
    private List<String> fields;

    @Option(names = "origin.task", description = "List only files produced by task specified by ID in this field.")
    private List<String> originTasks;

    @Option(names = "origin.dataset", description = "List only files which are part of the dataset specified in this field.")
    private List<String> originDataSet;

    @Option(names = "tag", description = "List files containing this tag. ")
    private List<String> tags;

    private Map<String, List<String>> metadata;

    public static class MetadataArgs {
        @Option(names = "metadata", description = "List only files with that have the specified value in metadata field.")
        private List<String> metadata;
    }

    public MetadataArgs createMetadataArgs() {
        return new MetadataArgs();
    }

    /**
     * picocli doesn't support MultiMaps - hence the additional step
     */
    public CommandLineArgs applyMetadataArgs(MetadataArgs args) {

        if (args == null || args.metadata == null) {
            return new CommandLineArgs(this);
        }

        CommandLineArgs commandLineArgs = new CommandLineArgs(this);
        commandLineArgs.metadata = toMetadataMap(args);
        return commandLineArgs;
    }

    private Map<String, List<String>> toMetadataMap(MetadataArgs args) {

        Map<String, List<String>> metadata = new HashMap<>();
        for (String arg : args.metadata) {
            String[] split = arg.split("=");

            if (split.length != 2) {
                throw new IllegalArgumentException("metadata." + arg + " not recognized as a valid argument");
            }

            metadata.computeIfAbsent("metadata." + split[0], k -> new ArrayList<>()).add(split[1]);
        }
        return metadata;
    }

    public CommandLineArgs() {}

    public CommandLineArgs(CommandLineArgs commandLineArgs) {
        this(commandLineArgs.token, commandLineArgs.domain, commandLineArgs.operation, commandLineArgs.file,
                commandLineArgs.project, commandLineArgs.parent, commandLineArgs.destination, commandLineArgs.names,
                commandLineArgs.limit, commandLineArgs.offset, commandLineArgs.fields, commandLineArgs.originTasks,
                commandLineArgs.originDataSet, commandLineArgs.tags, commandLineArgs.metadata);
    }

    public CommandLineArgs(String token, Domain domain, Operation operation, String file, String project, String parent,
                           String destination, List<String> names, Integer limit, Integer offset, List<String> fields,
                           List<String> originTasks, List<String> originDataSet, List<String> tags, Map<String,
            List<String>> metadata) {

        this.token = token;
        this.domain = domain;
        this.operation = operation;
        this.file = file;
        this.project = project;
        this.parent = parent;
        this.destination = destination;
        this.names = names != null ? new ArrayList<>(names) : null;
        this.limit = limit;
        this.offset = offset;
        this.fields = fields != null ? new ArrayList<>(fields) : null;
        this.originTasks = originTasks != null ? new ArrayList<>(originTasks) : null;
        this.originDataSet = originDataSet != null ? new ArrayList<>(originDataSet) : null;
        this.tags = tags != null ? new ArrayList<>(tags) : null;

        this.metadata = metadata == null ? null : metadata.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> new ArrayList<>(e.getValue())));
    }

    public String usage() {

        CommandLine.Help help = new CommandLine.Help(this);

        String synopsis = help.synopsis(help.synopsisHeadingLength()).trim();
        synopsis = synopsis + " [metadata.{field}=<value>]..." + System.lineSeparator();

        String optionList = help.optionList() + "\t  metadata.{field}=<value>\t\tSpecified value in metadata field";

        return new StringBuilder().append(help.synopsisHeading())
                .append(synopsis)
                .append(System.lineSeparator())
                .append(help.parameterListHeading())
                .append(help.parameterList())
                .append(help.optionListHeading())
                .append(optionList)
                .toString();
    }
}
