package com.sbg.cli.tool;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

public class CollectionUtils {

    public static <K, V> Map<K, V> expandMap(Map<K, List<V>> map, Function<K, K> remapKeyFunction, Function<List<V>, V> remapValueFunction) {

        return map.entrySet().stream()
                        .collect(Collectors.toMap(
                                e -> remapKeyFunction.apply(e.getKey()),
                                e -> remapValueFunction.apply(e.getValue()))
                        );
    }
}
