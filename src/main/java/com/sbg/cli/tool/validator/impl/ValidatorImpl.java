package com.sbg.cli.tool.validator.impl;

import com.sbg.cli.tool.command.line.CommandLineArgs;
import com.sbg.cli.tool.validator.api.Validator;

import java.util.List;
import java.util.Map;

public class ValidatorImpl implements Validator {

    @Override
    public void validate(CommandLineArgs commandLineArgs) {

        if (commandLineArgs.getDomain() == null) {
            throw new IllegalStateException("Domain not specified.");
        }

        switch (commandLineArgs.getDomain()) {
            case projects:
                validateProjectsCommand(commandLineArgs);
                break;
            case files:
                validateFilesCommand(commandLineArgs);
                break;
            default:
                throw new IllegalArgumentException("Unrecognized " + commandLineArgs.getDomain() + " domain.");
        }
    }

    private void validateProjectsCommand(CommandLineArgs commandLineArgs) {
        if (commandLineArgs.getOperation() != CommandLineArgs.Operation.list) {
            throw new IllegalArgumentException("Operation " + commandLineArgs.getOperation() + " not allowed on projects.");
        }

        // TODO: Check for unsupported options on the command line
    }

    private void validateFilesCommand(CommandLineArgs commandLineArgs) {

        if (commandLineArgs.getOperation() == null) {
            throw new IllegalStateException("Operation not specified.");
        }

        switch (commandLineArgs.getOperation()) {
            case list:
                validateFilesList(commandLineArgs);
                break;
            case stat:
                validateFilesStatCommand(commandLineArgs);
                break;
            case update:
                validateFilesUpdateCommand(commandLineArgs);
                break;
            case download:
                validateFilesDownloadCommand(commandLineArgs);
                break;
            default:
                throw new IllegalArgumentException("Operation " + commandLineArgs.getOperation() + " not allowed on files.");
        }
    }

    private void validateFilesList(CommandLineArgs commandLineArgs) {
        if (commandLineArgs.getProject() != null && commandLineArgs.getParent() != null) {
            throw new IllegalStateException("Both project and parent are defined for 'files list' command.");
        }
        if (commandLineArgs.getProject() == null && commandLineArgs.getParent() == null) {
            throw new IllegalStateException("Neither project and parent are defined for 'files list' command.");
        }
        // TODO: Check for unsupported options on the command line
    }

    private void validateFilesStatCommand(CommandLineArgs commandLineArgs) {
        if (commandLineArgs.getFile() == null) {
            throw new IllegalStateException("File id not specified for command 'files stat' command.");
        }
        // TODO: Check for unsupported options on the command line
    }

    private void validateFilesUpdateCommand(CommandLineArgs commandLineArgs) {
        if (commandLineArgs.getFile() == null) {
            throw new IllegalStateException("File id not specified for command 'files update' command.");
        }
        if (commandLineArgs.getNames() == null && commandLineArgs.getMetadata() == null && commandLineArgs.getTags() == null) {
            throw new IllegalStateException("No details specified for 'files update' command.");
        }
        if (commandLineArgs.getNames() != null && !commandLineArgs.getNames().isEmpty() && commandLineArgs.getNames().size() != 1) {
            throw new IllegalArgumentException("Multiple 'name' options specified for 'files update command.'");
        }
        if (commandLineArgs.getMetadata() != null) {
            for (Map.Entry<String, List<String>> entry : commandLineArgs.getMetadata().entrySet()) {
                if (entry.getValue().size() != 1) {
                    throw new IllegalArgumentException("Multiple " + entry.getKey() + " values specified for 'files update' command.");
                }
            }
        }
        // TODO: Check for unsupported options on the command line
    }

    private void validateFilesDownloadCommand(CommandLineArgs commandLineArgs) {
        if (commandLineArgs.getFile() == null) {
            throw new IllegalStateException("File id not specified for command 'files download' command.");
        }
        if (commandLineArgs.getDestination() == null) {
            throw new IllegalStateException("Destination not specified for command 'files download' command.");
        }
        // TODO: Check for unsupported options on the command line
    }
}
