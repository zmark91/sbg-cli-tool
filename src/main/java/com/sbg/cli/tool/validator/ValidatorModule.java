package com.sbg.cli.tool.validator;

import com.google.inject.AbstractModule;
import com.sbg.cli.tool.validator.api.Validator;
import com.sbg.cli.tool.validator.impl.ValidatorImpl;

public class ValidatorModule extends AbstractModule {

    @Override
    protected void configure() {
        super.configure();

        bind(Validator.class).to(ValidatorImpl.class);
    }
}
