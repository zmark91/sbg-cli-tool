package com.sbg.cli.tool.validator.api;

import com.sbg.cli.tool.command.line.CommandLineArgs;

public interface Validator {

    void validate(CommandLineArgs commandLineArgs);
}
