package com.sbg.cli.tool.operation.impl;

import com.google.inject.Inject;
import com.sbg.cli.tool.client.api.FilesClient;
import com.sbg.cli.tool.client.api.query.param.FieldsQueryParam;
import com.sbg.cli.tool.client.api.query.params.DownloadInfoQueryParams;
import com.sbg.cli.tool.command.line.CommandLineArgs;
import com.sbg.cli.tool.io.FileOutputStreamFactory;
import com.sbg.cli.tool.io.ReadableByteChannelFactory;
import com.sbg.cli.tool.operation.api.Operation;
import lombok.extern.slf4j.Slf4j;

import java.io.FileOutputStream;
import java.nio.channels.ReadableByteChannel;
import java.util.Map;

@Slf4j
public class FilesDownloadOperation implements Operation {

    private final FilesClient client;
    private final FileOutputStreamFactory fileOutputStreamFactory;
    private final ReadableByteChannelFactory readableByteChannelFactory;

    @Inject
    public FilesDownloadOperation(FilesClient client, FileOutputStreamFactory fileOutputStreamFactory, ReadableByteChannelFactory readableByteChannelFactory) {
        this.client = client;
        this.fileOutputStreamFactory = fileOutputStreamFactory;
        this.readableByteChannelFactory = readableByteChannelFactory;
    }

    @Override
    public void execute(CommandLineArgs commandLineArgs) throws Exception {

        String authToken = commandLineArgs.getToken();
        String fileId = commandLineArgs.getFile();
        String destination = commandLineArgs.getDestination();
        DownloadInfoQueryParams downloadInfoQueryParams = toQueryParams(commandLineArgs);

        Map downloadInfo = client.getDownloadInfo(authToken, fileId, downloadInfoQueryParams);

        String url = (String) downloadInfo.get("url");
        log.info("Downloading file {} from url: {}", fileId, url);

        try (FileOutputStream fileOutputStream = fileOutputStreamFactory.create(destination);
             ReadableByteChannel readableByteChannel = readableByteChannelFactory.create(url)) {

            fileOutputStream.getChannel().transferFrom(readableByteChannel, 0, Long.MAX_VALUE);
        }

        log.info("Finished downloading file to: {}", destination);
    }

    private DownloadInfoQueryParams toQueryParams(CommandLineArgs commandLineArgs) {
        FieldsQueryParam fieldsQueryParam = new FieldsQueryParam(commandLineArgs.getFields());
        return new DownloadInfoQueryParams(fieldsQueryParam);
    }
}
