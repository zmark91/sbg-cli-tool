package com.sbg.cli.tool.operation;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.sbg.cli.tool.client.ClientModule;
import com.sbg.cli.tool.io.IOModule;

public class OperationModule extends AbstractModule {

    @Override
    protected void configure() {
        super.configure();

        install(new IOModule());
        install(new ClientModule());
    }

    @Provides
    ObjectMapper provideObjectMapper() {
        return new ObjectMapper();
    }
}
