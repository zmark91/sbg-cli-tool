package com.sbg.cli.tool.operation.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.sbg.cli.tool.client.api.FilesClient;
import com.sbg.cli.tool.client.api.query.param.*;
import com.sbg.cli.tool.client.api.query.params.ListFilesQueryParams;
import com.sbg.cli.tool.client.api.query.params.ParentLFQueryParams;
import com.sbg.cli.tool.client.api.query.params.ProjectLFQueryParams;
import com.sbg.cli.tool.command.line.CommandLineArgs;
import com.sbg.cli.tool.operation.api.Operation;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class FilesListOperation implements Operation {

    private final FilesClient filesClient;
    private final ObjectMapper objectMapper;

    @Inject
    public FilesListOperation(FilesClient filesClient, ObjectMapper objectMapper) {
        this.filesClient = filesClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public void execute(CommandLineArgs commandLineArgs) throws Exception {

        String authToken = commandLineArgs.getToken();
        ListFilesQueryParams listFilesQueryParams = toQueryParams(commandLineArgs);

        log.info("Listing files...");

        Map files = filesClient.listFiles(authToken, listFilesQueryParams);

        String filesJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(files);
        log.info(filesJson);
    }

    private ListFilesQueryParams toQueryParams(CommandLineArgs commandLineArgs) {

        if (commandLineArgs.getProject() != null) {
            return toProjectLFQueryParams(commandLineArgs);
        } else if (commandLineArgs.getParent() != null) {
            return toParentLFQueryParams(commandLineArgs);
        } else {
            throw new IllegalStateException("Neither 'project' nor 'parent' are specified for 'files list' operation");
        }
    }

    private ProjectLFQueryParams toProjectLFQueryParams(CommandLineArgs commandLineArgs) {

        OffsetQueryParam offsetQueryParam = new OffsetQueryParam(commandLineArgs.getOffset());
        LimitQueryParam limitQueryParam = new LimitQueryParam(commandLineArgs.getLimit());
        FieldsQueryParam fieldsQueryParam = new FieldsQueryParam(commandLineArgs.getFields());
        NameQueryParam nameQueryParam = new NameQueryParam(commandLineArgs.getNames());
        TagQueryParam tagQueryParam = new TagQueryParam(commandLineArgs.getTags());
        MetadataQueryParam metadataQueryParam = new MetadataQueryParam(commandLineArgs.getMetadata());
        OriginQueryParam originQueryParam = new OriginQueryParam(commandLineArgs.getOriginTasks(), commandLineArgs.getOriginDataSet());
        ProjectQueryParam projectQueryParam = new ProjectQueryParam(commandLineArgs.getProject());

        return ProjectLFQueryParams.builder()
                .offsetQueryParam(offsetQueryParam)
                .limitQueryParam(limitQueryParam)
                .fieldsQueryParam(fieldsQueryParam)
                .nameQueryParam(nameQueryParam)
                .tagQueryParam(tagQueryParam)
                .metadataQueryParam(metadataQueryParam)
                .originQueryParam(originQueryParam)
                .projectQueryParam(projectQueryParam)
                .build();
    }

    private ParentLFQueryParams toParentLFQueryParams(CommandLineArgs commandLineArgs) {

        OffsetQueryParam offsetQueryParam = new OffsetQueryParam(commandLineArgs.getOffset());
        LimitQueryParam limitQueryParam = new LimitQueryParam(commandLineArgs.getLimit());
        FieldsQueryParam fieldsQueryParam = new FieldsQueryParam(commandLineArgs.getFields());
        NameQueryParam nameQueryParam = new NameQueryParam(commandLineArgs.getNames());
        TagQueryParam tagQueryParam = new TagQueryParam(commandLineArgs.getTags());
        MetadataQueryParam metadataQueryParam = new MetadataQueryParam(commandLineArgs.getMetadata());
        OriginQueryParam originQueryParam = new OriginQueryParam(commandLineArgs.getOriginTasks(), commandLineArgs.getOriginDataSet());
        ParentQueryParam parentQueryParam = new ParentQueryParam(commandLineArgs.getParent());

        return ParentLFQueryParams.builder()
                .offsetQueryParam(offsetQueryParam)
                .limitQueryParam(limitQueryParam)
                .fieldsQueryParam(fieldsQueryParam)
                .nameQueryParam(nameQueryParam)
                .tagQueryParam(tagQueryParam)
                .metadataQueryParam(metadataQueryParam)
                .originQueryParam(originQueryParam)
                .parentQueryParam(parentQueryParam)
                .build();
    }
}
