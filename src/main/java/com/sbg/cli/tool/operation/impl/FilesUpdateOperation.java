package com.sbg.cli.tool.operation.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.sbg.cli.tool.CollectionUtils;
import com.sbg.cli.tool.client.api.FilesClient;
import com.sbg.cli.tool.client.api.query.param.FieldsQueryParam;
import com.sbg.cli.tool.client.api.query.params.UpdateFileDetailsQueryParams;
import com.sbg.cli.tool.command.line.CommandLineArgs;
import com.sbg.cli.tool.operation.api.Operation;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
public class FilesUpdateOperation implements Operation {

    private final FilesClient client;
    private final ObjectMapper mapper;

    @Inject
    public FilesUpdateOperation(FilesClient client, ObjectMapper mapper) {
        this.client = client;
        this.mapper = mapper;
    }

    @Override
    public void execute(CommandLineArgs commandLineArgs) throws Exception {

        String authToken = commandLineArgs.getToken();
        String fileId = commandLineArgs.getFile();
        Map detailsMap = toDetailsMap(commandLineArgs);
        UpdateFileDetailsQueryParams updateFileDetailsQueryParams = toQueryParams(commandLineArgs);

        log.info("Updating file...");

        Map fileDetails = client.updateFileDetails(authToken, fileId, detailsMap, updateFileDetailsQueryParams);

        String fileDetailsJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(fileDetails);
        log.info(fileDetailsJson);
    }

    private Map toDetailsMap(CommandLineArgs commandLineArgs) {

        List<String> names = commandLineArgs.getNames();
        List<String> tags = commandLineArgs.getTags();
        Map<String, List<String>> metadata = commandLineArgs.getMetadata();

        HashMap<Object, Object> details = new HashMap<>();

        if (names != null) {
            details.put("name", names.get(0));
        }
        if (metadata != null) {
            details.put("metadata", CollectionUtils.expandMap(metadata, s -> s.split("\\.")[1], list -> list.get(0)));
        }
        if (tags != null) {
            details.put("tags", tags.toArray(new String[0]));
        }

        return details;
    }

    private UpdateFileDetailsQueryParams toQueryParams(CommandLineArgs commandLineArgs) {

        FieldsQueryParam fieldsQueryParam = new FieldsQueryParam(commandLineArgs.getFields());
        return new UpdateFileDetailsQueryParams(fieldsQueryParam);
    }
}
