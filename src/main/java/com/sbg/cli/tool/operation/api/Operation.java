package com.sbg.cli.tool.operation.api;

import com.sbg.cli.tool.command.line.CommandLineArgs;

public interface Operation {

    void execute(CommandLineArgs commandLineArgs) throws Exception;
}
