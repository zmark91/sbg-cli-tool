package com.sbg.cli.tool.operation.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.sbg.cli.tool.client.api.FilesClient;
import com.sbg.cli.tool.client.api.query.param.FieldsQueryParam;
import com.sbg.cli.tool.client.api.query.params.GetFileDetailsQueryParams;
import com.sbg.cli.tool.command.line.CommandLineArgs;
import com.sbg.cli.tool.operation.api.Operation;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class FilesStatOperation implements Operation {

    private final FilesClient filesClient;
    private final ObjectMapper objectMapper;

    @Inject
    public FilesStatOperation(FilesClient filesClient, ObjectMapper objectMapper) {
        this.filesClient = filesClient;
        this.objectMapper = objectMapper;
    }

    @Override
    public void execute(CommandLineArgs commandLineArgs) throws Exception {

        String authToken = commandLineArgs.getToken();
        String fileId = commandLineArgs.getFile();
        GetFileDetailsQueryParams getFileDetailsQueryParams = toQueryParams(commandLineArgs);

        log.info("Getting file statistics...");

        Map fileDetails = filesClient.getFileDetails(authToken, fileId, getFileDetailsQueryParams);

        String fileDetailsJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(fileDetails);
        log.info(fileDetailsJson);
    }

    private GetFileDetailsQueryParams toQueryParams(CommandLineArgs commandLineArgs) {
        FieldsQueryParam fieldsQueryParam = new FieldsQueryParam(commandLineArgs.getFields());
        return new GetFileDetailsQueryParams(fieldsQueryParam);
    }
}
