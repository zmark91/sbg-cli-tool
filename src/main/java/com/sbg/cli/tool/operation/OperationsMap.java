package com.sbg.cli.tool.operation;

import com.sbg.cli.tool.command.line.CommandLineArgs;
import com.sbg.cli.tool.operation.api.Operation;
import com.sbg.cli.tool.operation.impl.*;

public class OperationsMap {

    public Class<? extends Operation> operation(CommandLineArgs.Domain domain, CommandLineArgs.Operation operation) {
        switch (domain) {
            case projects:
                return ProjectsListOperation.class;
            case files:
                switch (operation) {
                    case list:
                        return FilesListOperation.class;
                    case download:
                        return FilesDownloadOperation.class;
                    case update:
                        return FilesUpdateOperation.class;
                    case stat:
                        return FilesStatOperation.class;
                    default:
                        throw new IllegalStateException("Unknown operation " + operation + " to perform upon files");
                }
            default:
                throw new IllegalStateException("Unknown domain "  + domain + " to work upon.");
        }
    }
}
