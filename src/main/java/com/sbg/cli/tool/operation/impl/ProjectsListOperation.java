package com.sbg.cli.tool.operation.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.sbg.cli.tool.client.api.ProjectsClient;
import com.sbg.cli.tool.client.api.query.param.FieldsQueryParam;
import com.sbg.cli.tool.client.api.query.param.LimitQueryParam;
import com.sbg.cli.tool.client.api.query.param.OffsetQueryParam;
import com.sbg.cli.tool.client.api.query.params.ListProjectsQueryParams;
import com.sbg.cli.tool.command.line.CommandLineArgs;
import com.sbg.cli.tool.operation.api.Operation;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class ProjectsListOperation implements Operation {

    private final ProjectsClient client;
    private final ObjectMapper mapper;

    @Inject
    public ProjectsListOperation(ProjectsClient client, ObjectMapper mapper) {
        this.client = client;
        this.mapper = mapper;
    }

    @Override
    public void execute(CommandLineArgs commandLineArgs) throws Exception {

        String authToken = commandLineArgs.getToken();
        ListProjectsQueryParams listProjectsQueryParams = toQueryParams(commandLineArgs);

        log.info("List projects...");

        Map projects = client.listProjects(authToken, listProjectsQueryParams);

        String projectsJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(projects);
        log.info(projectsJson);
    }

    private ListProjectsQueryParams toQueryParams(CommandLineArgs commandLineArgs) {

        OffsetQueryParam offsetQueryParam = new OffsetQueryParam(commandLineArgs.getOffset());
        LimitQueryParam limitQueryParam = new LimitQueryParam(commandLineArgs.getLimit());
        FieldsQueryParam fieldsQueryParam = new FieldsQueryParam(commandLineArgs.getFields());

        return ListProjectsQueryParams.builder()
                .offsetQueryParam(offsetQueryParam)
                .limitQueryParam(limitQueryParam)
                .fieldsQueryParam(fieldsQueryParam)
                .build();
    }
}
