package com.sbg.cli.tool;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.sbg.cli.tool.command.line.CommandLineArgs;
import com.sbg.cli.tool.operation.OperationModule;
import com.sbg.cli.tool.operation.OperationsMap;
import com.sbg.cli.tool.operation.api.Operation;
import com.sbg.cli.tool.parser.ParserModule;
import com.sbg.cli.tool.parser.api.Parser;
import com.sbg.cli.tool.validator.ValidatorModule;
import com.sbg.cli.tool.validator.api.Validator;
import lombok.extern.slf4j.Slf4j;

import java.util.Arrays;

@Slf4j
public class Main {

    public static void main(String[] args) throws Exception {
        try {

            Injector injector = Guice.createInjector(new ParserModule(), new ValidatorModule(), new OperationModule());

            Parser parser = injector.getInstance(Parser.class);
            CommandLineArgs commandLineArgs = parser.parse(args);

            Validator validator = injector.getInstance(Validator.class);
            validator.validate(commandLineArgs);

            OperationsMap operationsMap = injector.getInstance(OperationsMap.class);
            Class<? extends Operation> operationClass = operationsMap.operation(commandLineArgs.getDomain(), commandLineArgs.getOperation());

            Operation operation = injector.getInstance(operationClass);
            operation.execute(commandLineArgs);

        } catch (Exception e) {
            log.error("Error processing arguments: " + Arrays.toString(args), e);
            System.err.println(new CommandLineArgs().usage());
            throw e;
        }
    }
}
