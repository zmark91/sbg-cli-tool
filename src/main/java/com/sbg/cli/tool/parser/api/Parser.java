package com.sbg.cli.tool.parser.api;

import com.sbg.cli.tool.command.line.CommandLineArgs;

public interface Parser {

    CommandLineArgs parse(String[] args);
}
