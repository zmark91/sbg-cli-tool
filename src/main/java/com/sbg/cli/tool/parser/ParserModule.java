package com.sbg.cli.tool.parser;

import com.google.inject.AbstractModule;
import com.sbg.cli.tool.command.line.CommandLineModule;
import com.sbg.cli.tool.parser.api.Parser;
import com.sbg.cli.tool.parser.impl.ParserImpl;

public class ParserModule  extends AbstractModule {

    @Override
    protected void configure() {
        super.configure();

        bind(Parser.class).to(ParserImpl.class);

        install(new CommandLineModule());
    }
}
