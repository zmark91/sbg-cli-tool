package com.sbg.cli.tool.parser.impl;

import com.google.inject.Inject;
import com.google.inject.Provider;
import com.sbg.cli.tool.command.line.CommandLineArgs;
import com.sbg.cli.tool.command.line.CommandLineArgs.MetadataArgs;
import com.sbg.cli.tool.command.line.CommandLineFactory;
import com.sbg.cli.tool.parser.api.Parser;
import lombok.extern.slf4j.Slf4j;
import picocli.CommandLine.ParseResult;

import java.util.function.Supplier;

@Slf4j
public class ParserImpl implements Parser {

    private final Provider<CommandLineArgs> commandLineArgsProvider;
    private final CommandLineFactory commandLineFactory;

    @Inject
    public ParserImpl(Provider<CommandLineArgs> commandLineArgsProvider, CommandLineFactory commandLineFactory) {
        this.commandLineArgsProvider = commandLineArgsProvider;
        this.commandLineFactory = commandLineFactory;
    }

    @Override
    public CommandLineArgs parse(String[] args) {

        log.debug("Parsing arguments: {}", (Object) args);

        ParseResult parseResult = parse(args, "=", true, commandLineArgsProvider::get);
        CommandLineArgs commandLineArgs = fetchCommand(parseResult);

        if (!hasUnmatchedArgs(parseResult)) {
            return commandLineArgs;
        }

        String[] unmatchedArgs = fetchUnmatchedArgs(parseResult);
        log.debug("Re-parsing unmatched arguments: {}", (Object) unmatchedArgs);

        ParseResult result = parse(unmatchedArgs, ".", false, commandLineArgs::createMetadataArgs);
        MetadataArgs metadataArgs = fetchCommand(result);

        return commandLineArgs.applyMetadataArgs(metadataArgs);
    }

    private ParseResult parse(String[] args, String separator, boolean allowUnmatched, Supplier commandSupplier) {

        Object command = commandSupplier.get();
        return commandLineFactory.create(command)
                .setSeparator(separator)
                .setUnmatchedArgumentsAllowed(allowUnmatched)
                .parseArgs(args);
    }

    private <T> T fetchCommand(ParseResult parseResult) {
        return parseResult.commandSpec().commandLine().getCommand();
    }

    private boolean hasUnmatchedArgs(ParseResult parseResult) {
        return !parseResult.unmatched().isEmpty();
    }

    private String[] fetchUnmatchedArgs(ParseResult parseResult) {
        return parseResult.unmatched().toArray(new String[0]);
    }
}
