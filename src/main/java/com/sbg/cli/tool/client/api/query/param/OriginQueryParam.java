package com.sbg.cli.tool.client.api.query.param;

import com.sbg.cli.tool.client.api.query.Queryable;
import lombok.Builder;
import lombok.Singular;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Builder
public class OriginQueryParam implements Queryable {

    public static final String ORIGIN_TASK = "origin.task";
    public static final String ORIGIN_DATA_SET = "origin.dataset";

    @Singular
    private final List<String> tasks;
    @Singular
    private final List<String> dataSets;

    public OriginQueryParam(List<String> tasks, List<String> dataSets) {
        this.tasks = tasks != null ? new ArrayList<>(tasks) : new ArrayList<>();
        this.dataSets = dataSets != null ? new ArrayList<>(dataSets) : new ArrayList<>();
    }

    public OriginQueryParam() {
        this(new ArrayList<>(), new ArrayList<>());
    }

    @Override
    public Map<String, Object[]> queryParams() {

        HashMap<String, Object[]> parameters = new HashMap<>();

        if (!tasks.isEmpty()) {
            parameters.put(ORIGIN_TASK, tasks.toArray());
        }

        if (!dataSets.isEmpty()) {
            parameters.put(ORIGIN_DATA_SET, dataSets.toArray());
        }

        return parameters;
    }
}
