package com.sbg.cli.tool.client.api.query;

import java.util.Map;

public interface Queryable {

    Map<String, Object[]> queryParams();
}
