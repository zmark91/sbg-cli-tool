package com.sbg.cli.tool.client.impl.projects;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class ProjectsClientConfig {

    public static final String SERVER_IP = "server.ip-addr";
    public static final String AUTH_TOKEN = "server.auth-token";
    public static final String LIST_PROJECTS_PATH = "server.urls.projects.operation.list";

    private final String serverIp;
    private final String authToken;
    private final String listProjectsPath;

    public ProjectsClientConfig() {

        Config config = ConfigFactory.load().getConfig("com.sbg.cli-tool");

        serverIp = config.getString(SERVER_IP);
        authToken = config.getString(AUTH_TOKEN);
        listProjectsPath = config.getString(LIST_PROJECTS_PATH);
    }
}
