package com.sbg.cli.tool.client.api.query.param;

import com.sbg.cli.tool.client.api.query.Queryable;
import lombok.Builder;
import lombok.Singular;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Builder
public class TagQueryParam implements Queryable {

    public static final String TAG = "tag";

    @Singular
    private final List<String> tags;

    public TagQueryParam(List<String> tags) {
        this.tags = tags != null ? new ArrayList<>(tags) : new ArrayList<>();
    }

    public TagQueryParam() {
        this.tags = new ArrayList<>();
    }

    @Override
    public Map<String, Object[]> queryParams() {
        if (tags.isEmpty()) {
            return new HashMap<>();
        }

        Map<String, Object[]> params = new HashMap<>();
        params.put(TAG, tags.toArray());

        return params;
    }
}
