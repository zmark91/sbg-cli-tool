package com.sbg.cli.tool.client.api.query.params;

import com.sbg.cli.tool.client.api.query.param.*;

import java.util.HashMap;
import java.util.Map;

public abstract class ListFilesQueryParams extends BasicQueryParams {

    private final NameQueryParam nameQueryParam;
    private final MetadataQueryParam metadataQueryParam;
    private final OriginQueryParam originQueryParam;
    private final TagQueryParam tagQueryParam;

    protected ListFilesQueryParams(NameQueryParam nameQueryParam, MetadataQueryParam metadataQueryParam,
                                   OriginQueryParam originQueryParam, TagQueryParam tagQueryParam,
                                   OffsetQueryParam offsetQueryParam, LimitQueryParam limitQueryParam,
                                   FieldsQueryParam fieldsQueryParam) {

        super(offsetQueryParam, limitQueryParam, fieldsQueryParam);
        this.nameQueryParam = nameQueryParam;
        this.metadataQueryParam = metadataQueryParam;
        this.originQueryParam = originQueryParam;
        this.tagQueryParam = tagQueryParam;
    }

    @Override
    protected Map<String, Object[]> specificQueryParams() {

        Map<String, Object[]> rootPathQueryParam = rootPathQueryParam();
        Map<String, Object[]> params = new HashMap<>(rootPathQueryParam);

        if (nameQueryParam != null) {
            params.putAll(nameQueryParam.queryParams());
        }
        if (metadataQueryParam != null) {
            params.putAll(metadataQueryParam.queryParams());
        }
        if (originQueryParam != null) {
            params.putAll(originQueryParam.queryParams());
        }
        if (tagQueryParam != null) {
            params.putAll(tagQueryParam.queryParams());
        }

        return params;
    }

    protected abstract Map<String, Object[]> rootPathQueryParam();

}
