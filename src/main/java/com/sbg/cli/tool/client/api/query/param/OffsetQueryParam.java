package com.sbg.cli.tool.client.api.query.param;

import com.sbg.cli.tool.client.api.query.Queryable;

import java.util.HashMap;
import java.util.Map;

public class OffsetQueryParam implements Queryable {

    public static final String OFFSET = "offset";
    private final Integer offset;

    public OffsetQueryParam(Integer offset) {
        this.offset = offset;
    }

    public OffsetQueryParam() {
        this.offset = null;
    }

    @Override
    public Map<String, Object[]> queryParams() {
        if (offset == null) {
            return new HashMap<>();
        }

        Map<String, Object[]> params = new HashMap<>();
        params.put(OFFSET, new Integer[]{offset});

        return params;
    }
}
