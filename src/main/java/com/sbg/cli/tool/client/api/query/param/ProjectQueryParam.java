package com.sbg.cli.tool.client.api.query.param;

import com.sbg.cli.tool.client.api.query.Queryable;

import java.util.HashMap;
import java.util.Map;

public class ProjectQueryParam implements Queryable {

    public static final String PROJECT = "project";

    private final String project;

    public ProjectQueryParam(String project) {

        if (project == null || project.isEmpty()) {
            throw new IllegalArgumentException("Empty argument!");
        }

        this.project = project;
    }

    @Override
    public Map<String, Object[]> queryParams() {

        Map<String, Object[]> params = new HashMap<>();
        params.put(PROJECT, new String[]{project});

        return params;
    }
}
