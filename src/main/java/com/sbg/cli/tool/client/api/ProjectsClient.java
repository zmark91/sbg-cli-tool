package com.sbg.cli.tool.client.api;

import com.sbg.cli.tool.client.api.query.params.ListProjectsQueryParams;

import java.util.Map;

public interface ProjectsClient {

    Map listProjects(String authTokenValue, ListProjectsQueryParams queryParams);
}
