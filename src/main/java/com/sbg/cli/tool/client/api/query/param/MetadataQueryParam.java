package com.sbg.cli.tool.client.api.query.param;

import com.sbg.cli.tool.client.api.query.Queryable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MetadataQueryParam implements Queryable {

    private final Map<String, List<String>> fields;

    public MetadataQueryParam(Map<String, List<String>> fields) {
        this.fields = fields != null ? new HashMap<>(fields) : new HashMap<>();
    }

    public MetadataQueryParam() {
        this.fields = new HashMap<>();
    }

    @Override
    public Map<String, Object[]> queryParams() {

        if (fields.isEmpty()) {
            return new HashMap<>();
        }

        Map<String, Object[]> params = new HashMap<>();
        for (Map.Entry<String, List<String>> entry : fields.entrySet()) {
            params.put(entry.getKey(), entry.getValue().toArray());
        }

        return params;
    }


    public static MetadataQueryParamBuilder builder() {
        return new MetadataQueryParamBuilder();
    }

    public static class MetadataQueryParamBuilder {

        private Map<String, List<String>> fields = new HashMap<>();

        public MetadataQueryParam.MetadataQueryParamBuilder field(String fieldKey, String fieldValue) {

            this.fields.computeIfAbsent(fieldKey, k -> new ArrayList<>())
                    .add(fieldValue);

            return this;
        }

        public MetadataQueryParam.MetadataQueryParamBuilder fields(String fieldKey, List<String> fieldValues) {

            this.fields.computeIfAbsent(fieldKey, k -> new ArrayList<>())
                    .addAll(fieldValues);

            return this;
        }

        public MetadataQueryParam.MetadataQueryParamBuilder fields(Map<String, List<String>> fields) {

            for (Map.Entry<String, List<String>> entry : fields.entrySet()) {
                this.fields.computeIfAbsent(entry.getKey(), k -> new ArrayList<>())
                        .addAll(entry.getValue());
            }

            return this;
        }

        public MetadataQueryParam build() {
            return new MetadataQueryParam(fields);
        }
    }
}
