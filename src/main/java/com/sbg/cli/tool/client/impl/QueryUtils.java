package com.sbg.cli.tool.client.impl;

import com.sbg.cli.tool.client.api.query.Queryable;

import javax.ws.rs.client.WebTarget;
import java.util.Map;

public class QueryUtils {

    public static WebTarget applyQueryParams(WebTarget baseTarget, Queryable queryable) {

        if (queryable == null) {
            return baseTarget;
        }

        WebTarget target = baseTarget;
        for (Map.Entry<String, Object[]> entry : queryable.queryParams().entrySet()) {
            target = target.queryParam(entry.getKey(), entry.getValue());
        }

        return target;
    }
}