package com.sbg.cli.tool.client.api.query.param;

import com.sbg.cli.tool.client.api.query.Queryable;
import lombok.Builder;
import lombok.Singular;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Builder
public class NameQueryParam implements Queryable {

    public static final String NAME = "name";

    @Singular
    private final List<String> names;

    public NameQueryParam(List<String> names) {
        this.names = names != null ? new ArrayList<>(names) : new ArrayList<>();
    }

    public NameQueryParam() {
        this.names = new ArrayList<>();
    }

    @Override
    public Map<String, Object[]> queryParams() {
        if (names.isEmpty()) {
            return new HashMap<>();
        }

        Map<String, Object[]> params = new HashMap<>();
        params.put(NAME, names.toArray());

        return params;
    }
}
