package com.sbg.cli.tool.client.api;

import com.sbg.cli.tool.client.api.query.params.DownloadInfoQueryParams;
import com.sbg.cli.tool.client.api.query.params.GetFileDetailsQueryParams;
import com.sbg.cli.tool.client.api.query.params.ListFilesQueryParams;
import com.sbg.cli.tool.client.api.query.params.UpdateFileDetailsQueryParams;

import java.util.Map;

public interface FilesClient {

    Map listFiles(String authTokenValue, ListFilesQueryParams queryParams);

    Map getFileDetails(String authTokenValue, String file, GetFileDetailsQueryParams queryParams);

    Map updateFileDetails(String authTokenValue, String file, Map details, UpdateFileDetailsQueryParams queryParams);

    Map getDownloadInfo(String authTokenValue, String file, DownloadInfoQueryParams queryParams);
}
