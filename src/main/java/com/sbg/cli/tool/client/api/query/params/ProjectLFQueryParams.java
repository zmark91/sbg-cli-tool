package com.sbg.cli.tool.client.api.query.params;

import com.sbg.cli.tool.client.api.query.param.*;
import lombok.Builder;

import java.util.Map;

public class ProjectLFQueryParams extends ListFilesQueryParams {

    private final ProjectQueryParam projectQueryParam;

    @Builder
    public ProjectLFQueryParams(ProjectQueryParam projectQueryParam, NameQueryParam nameQueryParam, MetadataQueryParam metadataQueryParam,
                                OriginQueryParam originQueryParam, TagQueryParam tagQueryParam, OffsetQueryParam offsetQueryParam,
                                LimitQueryParam limitQueryParam, FieldsQueryParam fieldsQueryParam) {

        super(nameQueryParam, metadataQueryParam, originQueryParam, tagQueryParam, offsetQueryParam, limitQueryParam, fieldsQueryParam);

        if (projectQueryParam == null) {
            throw new IllegalArgumentException("Empty argument!");
        }
        this.projectQueryParam = projectQueryParam;
    }

    @Override
    protected Map<String, Object[]> rootPathQueryParam() {
        return projectQueryParam.queryParams();
    }
}
