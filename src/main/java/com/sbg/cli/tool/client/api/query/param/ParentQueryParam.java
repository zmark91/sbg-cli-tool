package com.sbg.cli.tool.client.api.query.param;

import com.sbg.cli.tool.client.api.query.Queryable;

import java.util.HashMap;
import java.util.Map;

public class ParentQueryParam implements Queryable {

    public static final String PARENT = "parent";

    private final String parent;

    public ParentQueryParam(String parent) {

        if (parent == null || parent.isEmpty()) {
            throw new IllegalArgumentException("Empty argument!");
        }

        this.parent = parent;
    }


    @Override
    public Map<String, Object[]> queryParams() {

        Map<String, Object[]> params = new HashMap<>();
        params.put(PARENT, new String[]{parent});

        return params;
    }
}
