package com.sbg.cli.tool.client.impl.files;

import com.google.inject.Inject;
import com.google.inject.assistedinject.Assisted;
import com.sbg.cli.tool.client.api.FilesClient;
import com.sbg.cli.tool.client.api.query.params.DownloadInfoQueryParams;
import com.sbg.cli.tool.client.api.query.params.GetFileDetailsQueryParams;
import com.sbg.cli.tool.client.api.query.params.ListFilesQueryParams;
import com.sbg.cli.tool.client.api.query.params.UpdateFileDetailsQueryParams;
import com.sbg.cli.tool.client.impl.QueryUtils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

public class FilesClientImpl implements FilesClient {

    private final Client client;
    private final FilesClientConfig config;

    @Inject
    public FilesClientImpl(Client client, FilesClientConfig config) {
        this.client = client;
        this.config = config;
    }

    @Override
    public Map listFiles(String authTokenValue, ListFilesQueryParams queryParams) {

        WebTarget listFilesTarget = client.target(config.getServerIp()).path(config.getListFilesPath());

        WebTarget target = QueryUtils.applyQueryParams(listFilesTarget, queryParams);

        return target.request(MediaType.APPLICATION_JSON_TYPE)
                .header(config.getAuthToken(), authTokenValue)
                .get(HashMap.class);
    }

    @Override
    public Map getFileDetails(String authTokenValue, String file, GetFileDetailsQueryParams queryParams) {

        WebTarget fileDetailsPath = client.target(config.getServerIp())
                .path(config.getFileDetailsPath())
                .resolveTemplate(config.getFileIdTemplate(), file);

        WebTarget target = QueryUtils.applyQueryParams(fileDetailsPath, queryParams);

        return target.request(MediaType.APPLICATION_JSON_TYPE)
                .header(config.getAuthToken(), authTokenValue)
                .get(HashMap.class);
    }

    @Override
    public Map updateFileDetails(String authTokenValue, String file, Map details, UpdateFileDetailsQueryParams queryParams) {

        WebTarget fileUpdatePath = client.target(config.getServerIp())
                .path(config.getFileUpdatePath())
                .resolveTemplate(config.getFileIdTemplate(), file);

        WebTarget target = QueryUtils.applyQueryParams(fileUpdatePath, queryParams);

        return target.request(MediaType.APPLICATION_JSON_TYPE)
                .header(config.getAuthToken(), authTokenValue)
                .method("PATCH", Entity.json(details))
                .readEntity(HashMap.class);
    }

    @Override
    public Map getDownloadInfo(String authTokenValue, String file, DownloadInfoQueryParams queryParams) {

        WebTarget downloadInfoPath = client.target(config.getServerIp())
                .path(config.getFileDownloadPath())
                .resolveTemplate(config.getFileIdTemplate(), file);

        WebTarget target = QueryUtils.applyQueryParams(downloadInfoPath, queryParams);

        return target.request(MediaType.APPLICATION_JSON_TYPE)
                .header(config.getAuthToken(), authTokenValue)
                .get(HashMap.class);
    }
}
