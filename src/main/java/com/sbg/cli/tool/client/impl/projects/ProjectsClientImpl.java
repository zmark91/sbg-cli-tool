package com.sbg.cli.tool.client.impl.projects;

import com.google.inject.Inject;
import com.sbg.cli.tool.client.api.ProjectsClient;
import com.sbg.cli.tool.client.api.query.params.ListProjectsQueryParams;
import com.sbg.cli.tool.client.impl.QueryUtils;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import java.util.HashMap;
import java.util.Map;

public class ProjectsClientImpl implements ProjectsClient {

    private final Client client;
    private final ProjectsClientConfig config;

    @Inject
    public ProjectsClientImpl(Client client, ProjectsClientConfig config) {
        this.client = client;
        this.config = config;
    }

    @Override
    public Map listProjects(String authTokenValue, ListProjectsQueryParams queryParams) {

        WebTarget listProjectsPath = client.target(config.getServerIp())
                .path(config.getListProjectsPath());

        WebTarget target = QueryUtils.applyQueryParams(listProjectsPath, queryParams);

        return target.request(MediaType.APPLICATION_JSON_TYPE)
                .header(config.getAuthToken(), authTokenValue)
                .get(HashMap.class);
    }
}
