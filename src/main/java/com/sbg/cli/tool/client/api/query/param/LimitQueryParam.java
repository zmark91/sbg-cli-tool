package com.sbg.cli.tool.client.api.query.param;

import com.sbg.cli.tool.client.api.query.Queryable;

import java.util.HashMap;
import java.util.Map;

public class LimitQueryParam implements Queryable {

    public static final String LIMIT = "limit";

    private final Integer limit;

    public LimitQueryParam(Integer limit) {
        this.limit = limit;
    }

    public LimitQueryParam() {
        limit = null;
    }

    @Override
    public Map<String, Object[]> queryParams() {
        if (limit == null) {
            return new HashMap<>();
        }

        Map<String, Object[]> params = new HashMap<>();
        params.put(LIMIT, new Integer[]{limit});

        return params;
    }
}
