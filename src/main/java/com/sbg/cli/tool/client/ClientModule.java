package com.sbg.cli.tool.client;

import com.google.inject.AbstractModule;
import com.google.inject.Provides;
import com.sbg.cli.tool.client.api.FilesClient;
import com.sbg.cli.tool.client.api.ProjectsClient;
import com.sbg.cli.tool.client.impl.files.FilesClientImpl;
import com.sbg.cli.tool.client.impl.projects.ProjectsClientImpl;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;

public class ClientModule extends AbstractModule {

    @Override
    protected void configure() {
        super.configure();

        bind(ProjectsClient.class).to(ProjectsClientImpl.class);
        bind(FilesClient.class).to(FilesClientImpl.class);
    }

    @Provides
    Client provideClient() {
        return ClientBuilder.newClient();
    }
}
