package com.sbg.cli.tool.client.api.query.params;

import com.sbg.cli.tool.client.api.query.Queryable;
import com.sbg.cli.tool.client.api.query.param.FieldsQueryParam;

import java.util.HashMap;
import java.util.Map;

public class GetFileDetailsQueryParams implements Queryable {

    private final FieldsQueryParam fieldsQueryParam;

    public GetFileDetailsQueryParams(FieldsQueryParam fieldsQueryParam) {
        this.fieldsQueryParam = fieldsQueryParam;
    }

    public GetFileDetailsQueryParams() {
        this.fieldsQueryParam = null;
    }

    @Override
    public Map<String, Object[]> queryParams() {
        if (fieldsQueryParam == null) {
            return new HashMap<>();
        }

        return fieldsQueryParam.queryParams();
    }
}
