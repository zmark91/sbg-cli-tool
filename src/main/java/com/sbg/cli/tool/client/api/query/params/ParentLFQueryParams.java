package com.sbg.cli.tool.client.api.query.params;

import com.sbg.cli.tool.client.api.query.param.*;
import lombok.Builder;

import java.util.Map;

public class ParentLFQueryParams extends ListFilesQueryParams {

    private final ParentQueryParam parentQueryParam;

    @Builder
    public ParentLFQueryParams(ParentQueryParam parentQueryParam, NameQueryParam nameQueryParam, MetadataQueryParam metadataQueryParam,
                               OriginQueryParam originQueryParam, TagQueryParam tagQueryParam, OffsetQueryParam offsetQueryParam,
                               LimitQueryParam limitQueryParam, FieldsQueryParam fieldsQueryParam) {

        super(nameQueryParam, metadataQueryParam, originQueryParam, tagQueryParam, offsetQueryParam, limitQueryParam, fieldsQueryParam);

        if (parentQueryParam == null) {
            throw new IllegalArgumentException("Empty argument!");
        }

        this.parentQueryParam = parentQueryParam;
    }

    @Override
    protected Map<String, Object[]> rootPathQueryParam() {
        return parentQueryParam.queryParams();
    }
}
