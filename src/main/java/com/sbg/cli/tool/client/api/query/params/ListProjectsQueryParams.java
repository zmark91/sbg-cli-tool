package com.sbg.cli.tool.client.api.query.params;

import com.sbg.cli.tool.client.api.query.param.FieldsQueryParam;
import com.sbg.cli.tool.client.api.query.param.LimitQueryParam;
import com.sbg.cli.tool.client.api.query.param.OffsetQueryParam;
import lombok.Builder;

import java.util.HashMap;
import java.util.Map;

public class ListProjectsQueryParams extends BasicQueryParams {

    @Builder
    protected ListProjectsQueryParams(OffsetQueryParam offsetQueryParam, LimitQueryParam limitQueryParam, FieldsQueryParam fieldsQueryParam) {
        super(offsetQueryParam, limitQueryParam, fieldsQueryParam);
    }

    @Override
    protected Map<String, Object[]> specificQueryParams() {
        return new HashMap<>();
    }
}
