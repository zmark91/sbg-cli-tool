package com.sbg.cli.tool.client.impl.files;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class FilesClientConfig {

    public static final String SERVER_IP = "server.ip-addr";
    public static final String AUTH_TOKEN = "server.auth-token";
    public static final String FILE_ID_TEMPLATE = "server.urls.files.file-id-template";
    public static final String FILES_LIST_PATH = "server.urls.files.operation.list";
    public static final String FILE_DETAILS_PATH = "server.urls.files.operation.file-details";
    public static final String FILE_UPDATE_PATH = "server.urls.files.operation.file-update";
    public static final String FILE_DOWNLOAD_PATH = "server.urls.files.operation.file-download";

    private final String serverIp;
    private final String authToken;
    private final String fileIdTemplate;
    private final String listFilesPath;
    private final String fileDetailsPath;
    private final String fileUpdatePath;
    private final String fileDownloadPath;

    public FilesClientConfig() {

        Config config = ConfigFactory.load().getConfig("com.sbg.cli-tool");

        serverIp = config.getString(SERVER_IP);
        authToken = config.getString(AUTH_TOKEN);
        fileIdTemplate = config.getString(FILE_ID_TEMPLATE);
        listFilesPath = config.getString(FILES_LIST_PATH);
        fileDetailsPath = config.getString(FILE_DETAILS_PATH);
        fileUpdatePath = config.getString(FILE_UPDATE_PATH);
        fileDownloadPath = config.getString(FILE_DOWNLOAD_PATH);
    }
}
