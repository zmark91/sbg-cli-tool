package com.sbg.cli.tool.client.api.query.param;

import com.sbg.cli.tool.client.api.query.Queryable;
import lombok.Builder;
import lombok.Singular;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Builder
public class FieldsQueryParam implements Queryable {

    public static final String FIELDS = "fields";

    @Singular
    private final List<String> fields;

    public FieldsQueryParam(List<String> fields) {
        this.fields = fields != null ? new ArrayList<>(fields) : new ArrayList<>();
    }

    public FieldsQueryParam() {
        this.fields = new ArrayList<>();
    }

    @Override
    public Map<String, Object[]> queryParams() {
        if (fields.isEmpty()) {
            return new HashMap<>();
        }

        Map<String, Object[]> params = new HashMap<>();

        String fieldsString = fields.stream().collect(Collectors.joining(","));

        params.put(FIELDS, new String[] {fieldsString});

        return params;
    }
}
