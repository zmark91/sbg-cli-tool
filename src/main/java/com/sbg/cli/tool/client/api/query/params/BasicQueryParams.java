package com.sbg.cli.tool.client.api.query.params;

import com.sbg.cli.tool.client.api.query.Queryable;
import com.sbg.cli.tool.client.api.query.param.FieldsQueryParam;
import com.sbg.cli.tool.client.api.query.param.LimitQueryParam;
import com.sbg.cli.tool.client.api.query.param.OffsetQueryParam;

import java.util.HashMap;
import java.util.Map;

public abstract class BasicQueryParams implements Queryable {

    private final FieldsQueryParam fieldsQueryParam;
    private final OffsetQueryParam offsetQueryParam;
    private final LimitQueryParam limitQueryParam;

    protected BasicQueryParams(OffsetQueryParam offsetQueryParam, LimitQueryParam limitQueryParam, FieldsQueryParam fieldsQueryParam) {
        this.offsetQueryParam = offsetQueryParam;
        this.limitQueryParam = limitQueryParam;
        this.fieldsQueryParam = fieldsQueryParam;
    }

    @Override
    public Map<String, Object[]> queryParams() {

        Map<String, Object[]> specificQueryParams = specificQueryParams();
        Map<String, Object[]> params = new HashMap<>(specificQueryParams);

        if (fieldsQueryParam != null) {
            params.putAll(fieldsQueryParam.queryParams());
        }
        if (offsetQueryParam != null) {
            params.putAll(offsetQueryParam.queryParams());
        }
        if (limitQueryParam != null) {
            params.putAll(limitQueryParam.queryParams());
        }

        return params;
    }

    protected abstract Map<String, Object[]> specificQueryParams();
}
