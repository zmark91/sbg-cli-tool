package com.sbg.cli.tool.io;

import java.io.FileOutputStream;

public interface FileOutputStreamFactory {

    FileOutputStream create(String fileName);
}
