package com.sbg.cli.tool.io;

import java.nio.channels.ReadableByteChannel;

public interface ReadableByteChannelFactory {

    ReadableByteChannel create(String url);
}
