package com.sbg.cli.tool.io;

import com.google.inject.AbstractModule;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;

public class IOModule extends AbstractModule {

    @Override
    protected void configure() {
        super.configure();

        bind(FileOutputStreamFactory.class).to(FileOutputStreamFactoryImpl.class);
        bind(ReadableByteChannelFactory.class).to(ReadableByteChannelFactoryImpl.class);
    }

    public static class FileOutputStreamFactoryImpl implements FileOutputStreamFactory {
        @Override
        public FileOutputStream create(String fileName) {
            try {
                return new FileOutputStream(fileName);
            } catch (FileNotFoundException e) {
                throw new IllegalStateException(e);
            }
        }
    }

    public static class ReadableByteChannelFactoryImpl implements ReadableByteChannelFactory {
        @Override
        public ReadableByteChannel create(String url) {
            try {
                return Channels.newChannel(new URL(url).openStream());
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        }
    }
}
