package com.sbg.cli.tool.client.api.query.param;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class ProjectQueryParamTest {

    @Test
    public void queryParams() {

        ProjectQueryParam projectQueryParam = new ProjectQueryParam("test");
        Map<String, Object[]> params = projectQueryParam.queryParams();

        Assert.assertArrayEquals(new String[] {"test"}, params.get(ProjectQueryParam.PROJECT));
    }

    @Test(expected = IllegalArgumentException.class)
    public void illegalProjectQueryParam() {
        ProjectQueryParam projectQueryParam = new ProjectQueryParam("");
    }
}