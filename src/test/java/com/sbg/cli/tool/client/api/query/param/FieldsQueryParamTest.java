package com.sbg.cli.tool.client.api.query.param;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class FieldsQueryParamTest {

    @Test
    public void queryParams() {

        FieldsQueryParam fieldsQueryParam = FieldsQueryParam.builder()
                .field("project")
                .field("origin")
                .build();

        Map<String, Object[]> params = fieldsQueryParam.queryParams();

        Assert.assertArrayEquals(new String [] {"project,origin"}, params.get(FieldsQueryParam.FIELDS));
    }

    @Test
    public void queryParamsEmpty() {
        FieldsQueryParam fieldsQueryParam = new FieldsQueryParam();
        Map<String, Object[]> params = fieldsQueryParam.queryParams();

        Assert.assertTrue(params.isEmpty());
    }
}