package com.sbg.cli.tool.client.api.query.param;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class OriginQueryParamTest {

    @Test
    public void queryParams() {

        OriginQueryParam originQueryParam = OriginQueryParam.builder()
                .dataSet("tcga")
                .dataSet("tcga_grch38")
                .task("some_task")
                .build();

        Map<String, Object[]> params = originQueryParam.queryParams();

        Assert.assertNotNull(params.get(OriginQueryParam.ORIGIN_DATA_SET));
        Assert.assertArrayEquals(new String[]{"tcga", "tcga_grch38"}, params.get(OriginQueryParam.ORIGIN_DATA_SET));

        Assert.assertNotNull(params.get(OriginQueryParam.ORIGIN_TASK));
        Assert.assertArrayEquals(new String[]{"some_task"}, params.get(OriginQueryParam.ORIGIN_TASK));
    }

    @Test
    public void queryParamsEmpty() {

        OriginQueryParam originQueryParam = new OriginQueryParam();
        Map<String, Object[]> params = originQueryParam.queryParams();

        Assert.assertTrue(params.isEmpty());
    }
}