package com.sbg.cli.tool.client.api.query.param;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

public class NameQueryParamTest {

    @Test
    public void queryParams() {

        NameQueryParam nameQueryParam = NameQueryParam.builder()
                .name("file1")
                .names(Arrays.asList("file2", "file3"))
                .build();

        Map<String, Object[]> params = nameQueryParam.queryParams();

        Assert.assertArrayEquals(new String[] {"file1", "file2", "file3"}, params.get(NameQueryParam.NAME));
    }

    @Test
    public void queryParamsEmpty() {

        NameQueryParam nameQueryParam = new NameQueryParam();
        Map<String, Object[]> params = nameQueryParam.queryParams();

        Assert.assertTrue(params.isEmpty());
    }
}