package com.sbg.cli.tool.client.api.query.params;

import com.sbg.cli.tool.client.api.query.param.FieldsQueryParam;
import com.sbg.cli.tool.client.api.query.param.MetadataQueryParam;
import com.sbg.cli.tool.client.api.query.param.ProjectQueryParam;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class ProjectLFQueryParamsTest {

    @Test
    public void queryParams() {

        ProjectQueryParam projectQueryParam = new ProjectQueryParam("project-name");

        MetadataQueryParam metadataQueryParam = MetadataQueryParam.builder()
                            .field("metadata.sample_id", "ERR999999")
                            .field("metadata.library_id", "HiSeqX_R")
                            .build();

        FieldsQueryParam fieldsQueryParam = FieldsQueryParam.builder()
                            .field("name")
                            .field("origin")
                            .build();

        ProjectLFQueryParams projectLFQueryParams = ProjectLFQueryParams.builder()
                .projectQueryParam(projectQueryParam)
                .metadataQueryParam(metadataQueryParam)
                .fieldsQueryParam(fieldsQueryParam)
                .build();

        Map<String, Object[]> params = projectLFQueryParams.queryParams();

        Assert.assertEquals(4, params.size());
        Assert.assertArrayEquals(new String[] {"project-name"}, params.get(ProjectQueryParam.PROJECT));
        Assert.assertArrayEquals(new String[] {"ERR999999"}, params.get("metadata.sample_id"));
        Assert.assertArrayEquals(new String[] {"HiSeqX_R"}, params.get("metadata.library_id"));
        Assert.assertArrayEquals(new String[] {"name,origin"}, params.get(FieldsQueryParam.FIELDS));
    }
}