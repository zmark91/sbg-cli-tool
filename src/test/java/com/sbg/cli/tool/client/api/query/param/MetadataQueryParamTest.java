package com.sbg.cli.tool.client.api.query.param;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class MetadataQueryParamTest {

    @Test
    public void queryParams() {

        MetadataQueryParam metadataQueryParam = MetadataQueryParam.builder()
                .field("metadata.sample_id", "ERR315335")
                .field("metadata.sample_id", "ERR999999")
                .field("metadata.library_id", "HiSeqX_R")
                .build();

        Map<String, Object[]> params = metadataQueryParam.queryParams();

        Assert.assertArrayEquals(new String[] {"ERR315335", "ERR999999"}, params.get("metadata.sample_id"));
        Assert.assertArrayEquals(new String[] {"HiSeqX_R"}, params.get("metadata.library_id"));
    }

    @Test
    public void queryParamsEmpty() {

        MetadataQueryParam metadataQueryParam = new MetadataQueryParam();
        Map<String, Object[]> params = metadataQueryParam.queryParams();
        Assert.assertTrue(params.isEmpty());
    }
}