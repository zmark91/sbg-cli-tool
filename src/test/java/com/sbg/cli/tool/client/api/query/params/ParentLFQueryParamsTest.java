package com.sbg.cli.tool.client.api.query.params;

import com.sbg.cli.tool.client.api.query.param.NameQueryParam;
import com.sbg.cli.tool.client.api.query.param.OffsetQueryParam;
import com.sbg.cli.tool.client.api.query.param.ParentQueryParam;
import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class ParentLFQueryParamsTest {

    @Test
    public void queryParams() {

        ParentQueryParam parentQueryParam = new ParentQueryParam("5c004b2fe4b0883298cb09cd");

        NameQueryParam nameQueryParam = NameQueryParam.builder()
                .name("test-name")
                .build();

        OffsetQueryParam offsetQueryParam = new OffsetQueryParam(10);

        ParentLFQueryParams parentLFQueryParams = ParentLFQueryParams.builder()
                .parentQueryParam(parentQueryParam)
                .nameQueryParam(nameQueryParam)
                .offsetQueryParam(offsetQueryParam)
                .build();

        Map<String, Object[]> queryParams = parentLFQueryParams.queryParams();

        Assert.assertEquals(3, queryParams.size());
        Assert.assertArrayEquals(new String[] {"5c004b2fe4b0883298cb09cd"}, queryParams.get(ParentQueryParam.PARENT));
        Assert.assertArrayEquals(new String[] {"test-name"}, queryParams.get(NameQueryParam.NAME));
        Assert.assertArrayEquals(new Integer[] {10}, queryParams.get(OffsetQueryParam.OFFSET));
    }

    @Test(expected = IllegalArgumentException.class)
    public void illegalParentLFQueryParams() {
        ParentLFQueryParams.builder().build();
    }
}