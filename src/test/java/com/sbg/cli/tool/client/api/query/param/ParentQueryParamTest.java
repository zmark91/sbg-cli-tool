package com.sbg.cli.tool.client.api.query.param;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class ParentQueryParamTest {

    @Test
    public void queryParams() {

        ParentQueryParam parentQueryParam = new ParentQueryParam("5c004b2fe4b0883298cb09cd");
        Map<String, Object[]> params = parentQueryParam.queryParams();

        Assert.assertArrayEquals(new String[] {"5c004b2fe4b0883298cb09cd"}, params.get(ParentQueryParam.PARENT));
    }

    @Test(expected = IllegalArgumentException.class)
    public void illegalQueryParam() {
        ParentQueryParam parentQueryParam = new ParentQueryParam(null);
    }
}