package com.sbg.cli.tool.client.api.query.param;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class LimitQueryParamTest {

    @Test
    public void queryParams() {

        LimitQueryParam limitQueryParam = new LimitQueryParam(10);

        Map<String, Object[]> params = limitQueryParam.queryParams();

        Assert.assertNotNull(params.get(LimitQueryParam.LIMIT));
        Assert.assertArrayEquals(new Integer[] {10}, params.get(LimitQueryParam.LIMIT));
    }

    @Test
    public void queryParamsEmpty() {

        LimitQueryParam limitQueryParam = new LimitQueryParam();

        Map<String, Object[]> params = limitQueryParam.queryParams();

        Assert.assertTrue(params.isEmpty());
    }
}