package com.sbg.cli.tool.client.api.query.param;

import org.junit.Assert;
import org.junit.Test;

import java.util.Map;

public class OffsetQueryParamTest {

    @Test
    public void queryParams() {

        OffsetQueryParam offsetQueryParam = new OffsetQueryParam(1);
        Map<String, Object[]> params = offsetQueryParam.queryParams();

        Assert.assertArrayEquals(new Integer[]{1}, params.get(OffsetQueryParam.OFFSET));
    }

    @Test
    public void queryParamsEmpty() {
        OffsetQueryParam offsetQueryParam = new OffsetQueryParam();
        Map<String, Object[]> params = offsetQueryParam.queryParams();

        Assert.assertTrue(params.isEmpty());
    }
}