package com.sbg.cli.tool.client.api.query.param;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.Map;

public class TagQueryParamTest {

    @Test
    public void queryParams() {

        TagQueryParam tagQueryParam = TagQueryParam.builder()
                .tag("tag1")
                .tags(Arrays.asList("tag2", "tag3"))
                .build();

        Map<String, Object[]> params = tagQueryParam.queryParams();

        Assert.assertArrayEquals(new String[]{"tag1", "tag2", "tag3"}, params.get(TagQueryParam.TAG));
    }

    @Test
    public void queryParamsEmpty() {

        TagQueryParam tagQueryParam = new TagQueryParam();
        Map<String, Object[]> params = tagQueryParam.queryParams();

        Assert.assertTrue(params.isEmpty());
    }
}