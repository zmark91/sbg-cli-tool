package com.sbg.cli.tool.validator.impl;

import com.sbg.cli.tool.command.line.CommandLineArgs;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidatorImplTest {

    @Test(expected = IllegalArgumentException.class)
    public void validateProjectsWrongOperation() {
        CommandLineArgs commandLineArgs = CommandLineArgs.builder()
                .domain(CommandLineArgs.Domain.projects)
                .operation(CommandLineArgs.Operation.update)
                .build();

        new ValidatorImpl().validate(commandLineArgs);
    }

    @Test(expected = IllegalStateException.class)
    public void validateFilesListMultipleRoots() {
        CommandLineArgs commandLineArgs = CommandLineArgs.builder()
                .domain(CommandLineArgs.Domain.files)
                .operation(CommandLineArgs.Operation.list)
                .project("project")
                .parent("parent")
                .build();

        new ValidatorImpl().validate(commandLineArgs);
    }

    @Test(expected = IllegalStateException.class)
    public void validateFilesListNoRoot() {
        CommandLineArgs commandLineArgs = CommandLineArgs.builder()
                .domain(CommandLineArgs.Domain.files)
                .operation(CommandLineArgs.Operation.list)
                .build();

        new ValidatorImpl().validate(commandLineArgs);
    }

    @Test(expected = IllegalStateException.class)
    public void validateFilesStatNofFileId() {
        CommandLineArgs commandLineArgs = CommandLineArgs.builder()
                .domain(CommandLineArgs.Domain.files)
                .operation(CommandLineArgs.Operation.stat)
                .build();

        new ValidatorImpl().validate(commandLineArgs);
    }

    @Test(expected = IllegalStateException.class)
    public void validateFilesUpdateNoFileId() {
        CommandLineArgs commandLineArgs = CommandLineArgs.builder()
                .domain(CommandLineArgs.Domain.files)
                .operation(CommandLineArgs.Operation.update)
                .build();

        new ValidatorImpl().validate(commandLineArgs);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateFilesUpdateMultipleNames() {
        CommandLineArgs commandLineArgs = CommandLineArgs.builder()
                .domain(CommandLineArgs.Domain.files)
                .operation(CommandLineArgs.Operation.update)
                .file("file_1")
                .names(Arrays.asList("name1", "name2"))
                .build();

        new ValidatorImpl().validate(commandLineArgs);
    }

    @Test(expected = IllegalStateException.class)
    public void validateFilesUpdateNoDetails() {
        CommandLineArgs commandLineArgs = CommandLineArgs.builder()
                .domain(CommandLineArgs.Domain.files)
                .operation(CommandLineArgs.Operation.update)
                .file("file_1")
                .build();

        new ValidatorImpl().validate(commandLineArgs);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateFilesUpdateMultipleMetadataField() {

        Map<String, List<String>> metadata = new HashMap<>();
        metadata.put("metadata.sample_id", Arrays.asList("asdasf", "AAAAAA"));

        CommandLineArgs commandLineArgs = CommandLineArgs.builder()
                .domain(CommandLineArgs.Domain.files)
                .operation(CommandLineArgs.Operation.update)
                .file("file_1")
                .metadata(metadata)
                .build();

        new ValidatorImpl().validate(commandLineArgs);
    }

    @Test(expected = IllegalStateException.class)
    public void validateFilesDownloadNoFileId() {
        CommandLineArgs commandLineArgs = CommandLineArgs.builder()
                .domain(CommandLineArgs.Domain.files)
                .operation(CommandLineArgs.Operation.download)
                .build();

        new ValidatorImpl().validate(commandLineArgs);
    }

    @Test(expected = IllegalStateException.class)
    public void validateFilesDownloadNoDestination() {
        CommandLineArgs commandLineArgs = CommandLineArgs.builder()
                .domain(CommandLineArgs.Domain.files)
                .file("file_id")
                .operation(CommandLineArgs.Operation.download)
                .build();

        new ValidatorImpl().validate(commandLineArgs);
    }

}