package com.sbg.cli.tool.parser;

import com.sbg.cli.tool.command.line.CommandLineArgs;
import com.sbg.cli.tool.parser.api.Parser;
import com.sbg.cli.tool.parser.impl.ParserImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import picocli.CommandLine;

import java.util.Arrays;

public class ParserImplTest {

    private Parser parser;

    @Before
    public void setUp() {
        parser = new ParserImpl(CommandLineArgs::new, CommandLine::new);
    }

    @Test
    public void parseProjectsList() {

        String[] args = {"--token", "{token}", "projects", "list"};
        CommandLineArgs commandLineArgs = parser.parse(args);

        Assert.assertEquals("{token}", commandLineArgs.getToken());
        Assert.assertEquals(CommandLineArgs.Domain.projects, commandLineArgs.getDomain());
        Assert.assertEquals(CommandLineArgs.Operation.list, commandLineArgs.getOperation());
    }

    @Test
    public void parseFilesList() {
        String[] args = {"--token", "{token}", "files", "list", "--project",
                "test/simons-genome-diversity-project-sgdp"};

        CommandLineArgs commandLineArgs = parser.parse(args);

        Assert.assertEquals("{token}", commandLineArgs.getToken());
        Assert.assertEquals(CommandLineArgs.Domain.files, commandLineArgs.getDomain());
        Assert.assertEquals(CommandLineArgs.Operation.list, commandLineArgs.getOperation());
        Assert.assertEquals("test/simons-genome-diversity-project-sgdp", commandLineArgs.getProject());
    }

    @Test
    public void parseFilesStat() {
        String[] args = {"--token", "{token}", "files", "stat", "--file",
                "{file_id}"};

        CommandLineArgs commandLineArgs = parser.parse(args);

        Assert.assertEquals("{token}", commandLineArgs.getToken());
        Assert.assertEquals(CommandLineArgs.Domain.files, commandLineArgs.getDomain());
        Assert.assertEquals(CommandLineArgs.Operation.stat, commandLineArgs.getOperation());
        Assert.assertEquals("{file_id}", commandLineArgs.getFile());
    }

    @Test
    public void parseFilesUpdate() {
        String[] args = {"--token", "{token}", "files", "update", "--file",
                "{file_id}", "name=bla"};

        CommandLineArgs commandLineArgs = parser.parse(args);

        Assert.assertEquals("{token}", commandLineArgs.getToken());
        Assert.assertEquals(CommandLineArgs.Domain.files, commandLineArgs.getDomain());
        Assert.assertEquals(CommandLineArgs.Operation.update, commandLineArgs.getOperation());
        Assert.assertEquals("{file_id}", commandLineArgs.getFile());
        Assert.assertEquals(Arrays.asList("bla"), commandLineArgs.getNames());
    }

    @Test
    public void parseFilesUpdate2() {
        String[] args = {"--token", "{token}", "files", "update", "--file",
                "{file_id}", "metadata.sample_id=asdasf"};

        CommandLineArgs commandLineArgs = parser.parse(args);

        Assert.assertEquals("{token}", commandLineArgs.getToken());
        Assert.assertEquals(CommandLineArgs.Domain.files, commandLineArgs.getDomain());
        Assert.assertEquals(CommandLineArgs.Operation.update, commandLineArgs.getOperation());
        Assert.assertEquals("{file_id}", commandLineArgs.getFile());
        Assert.assertEquals(Arrays.asList("asdasf"), commandLineArgs.getMetadata().get("metadata.sample_id"));
    }

    @Test
    public void parseFilesDownload() {
        String[] args = {"--token", "{token}", "files", "download", "--file",
                "{file_id}", "--dest", "/tmp/foo.bar"};

        CommandLineArgs commandLineArgs = parser.parse(args);

        Assert.assertEquals("{token}", commandLineArgs.getToken());
        Assert.assertEquals(CommandLineArgs.Domain.files, commandLineArgs.getDomain());
        Assert.assertEquals(CommandLineArgs.Operation.download, commandLineArgs.getOperation());
        Assert.assertEquals("{file_id}", commandLineArgs.getFile());
        Assert.assertEquals("/tmp/foo.bar", commandLineArgs.getDestination());
    }

    @Test
    public void parseMetadataArgs() {

        String[] args = {"--token", "{token}", "files", "update", "--file",
                "{file_id}", "metadata.sample_id=asdasf", "metadata.sample_id=ABCDEFG", "metadata.library_id=library_id"};

        CommandLineArgs commandLineArgs = parser.parse(args);

        Assert.assertEquals("{token}", commandLineArgs.getToken());
        Assert.assertEquals(CommandLineArgs.Domain.files, commandLineArgs.getDomain());
        Assert.assertEquals(CommandLineArgs.Operation.update, commandLineArgs.getOperation());
        Assert.assertEquals("{file_id}", commandLineArgs.getFile());

        Assert.assertEquals(Arrays.asList("asdasf", "ABCDEFG"), commandLineArgs.getMetadata().get("metadata.sample_id"));
        Assert.assertEquals(Arrays.asList("library_id"), commandLineArgs.getMetadata().get("metadata.library_id"));
    }

    @Test(expected = CommandLine.MissingParameterException.class)
    public void parseIncompleteArgs() {
        String[] args = {};
        parser.parse(args);
    }

    @Test(expected = CommandLine.UnmatchedArgumentException.class)
    public void parseUnknownArgs() {
        String[] args = {"--token", "{token}", "files", "update", "unknown"};

        parser.parse(args);
    }
}